package com.example.jaime.actividad2.model;

/**
 * Created by Jaime on 26/11/2017.
 */

public interface FirebaseListener {
    void userSignedIn();
}
