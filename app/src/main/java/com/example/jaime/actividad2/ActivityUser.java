package com.example.jaime.actividad2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.example.jaime.actividad2.model.FirebaseAdmin;

import static android.content.ContentValues.TAG;

public class ActivityUser extends AppCompatActivity {
    private TextView display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        display = (TextView) findViewById(R.id.textDisplay);
        display.setText("Hello " + FirebaseAdmin.getInstance().getCurrentUser().getEmail().toString() + "!!!");
        Log.v(TAG,"E-MAIL: " + FirebaseAdmin.getInstance().getCurrentUser().getEmail().toString());
        Log.v(TAG,"UID: " + FirebaseAdmin.getInstance().getCurrentUser().getUid().toString());
    }
}
