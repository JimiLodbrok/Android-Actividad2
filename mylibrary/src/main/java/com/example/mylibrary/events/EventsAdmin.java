package com.example.mylibrary.events;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.EventListener;

/**
 * Created by Jaime on 25/11/2017.
 */

public class EventsAdmin implements View.OnClickListener {
    private static EventsAdmin instance = new EventsAdmin();
    //private ArrayList<EventsListener> arrListeners;
    private EventsListener listener;
    private static final String LOGTAG = "EventsAdmin";

    /*
    public EventsAdmin() {
        arrListeners = new ArrayList<>();
    }
    */
    public static EventsAdmin getInstance() {
        return instance;
    }

    public void addListener(EventsListener listener) {
        this.listener = listener;
        //if (arrListeners.indexOf(listener) == -1)
        //  arrListeners.add(listener);
    }

    @Override
    public void onClick(View v) {
        //Log.e(LOGTAG, String.valueOf(arrListeners.size()));
        //for (EventsListener listener : arrListeners) {
        if (listener != null)
            listener.onClickScren(v);
    }
}