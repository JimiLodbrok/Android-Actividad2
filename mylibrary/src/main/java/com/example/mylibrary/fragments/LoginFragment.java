package com.example.mylibrary.fragments;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.example.mylibrary.R;
import com.example.mylibrary.events.EventsAdmin;
import com.example.mylibrary.events.EventsListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    private Button btnLogin, btnRegister;
    private TextInputEditText edtxt_User, edtxt_Password;
    private EventsListener listener;

    public LoginFragment() {
        // Required empty public constructor
    }

    public void setListener(EventsListener listener) {
        this.listener = listener;
        EventsAdmin.getInstance().addListener(listener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        btnLogin = (Button) v.findViewById(R.id.btnLogin);
        btnRegister = (Button) v.findViewById(R.id.btnRegister);

        edtxt_User = (TextInputEditText) v.findViewById(R.id.edtxt_user);
        edtxt_Password = (TextInputEditText) v.findViewById(R.id.edtxt_pass);

        btnRegister.setOnClickListener( EventsAdmin.getInstance());
        btnLogin.setOnClickListener( EventsAdmin.getInstance());
        return v;
    }

    public TextInputEditText getEdtxt_User() {
        return edtxt_User;
    }

    public TextInputEditText getEdtxt_Password() {
        return edtxt_Password;
    }
}
